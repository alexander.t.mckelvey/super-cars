from django.contrib import admin
from .models import AutoMobileVO, SalesPerson, PotentialCustomer, SaleRecord

# Register your models here.
admin.site.register(AutoMobileVO)
admin.site.register(SalesPerson)
admin.site.register(PotentialCustomer)
admin.site.register(SaleRecord)
