from django.db import models

# Create your models here.


class AutoMobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveIntegerField()

    def __str__(self):
        return self.name


class PotentialCustomer(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)
    phone_number = models.CharField(max_length=12)

    def __str__(self):
        return self.name

class SaleRecord(models.Model):
    automobile = models.ForeignKey(
        AutoMobileVO,
        on_delete=models.CASCADE,
        related_name="sale_records",
    )
    sales_person = models.ForeignKey(
        SalesPerson,
        on_delete=models.CASCADE,
        related_name="sale_records",
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        on_delete=models.CASCADE,
        related_name="sale_records",
    )
    sale_price = models.DecimalField(max_digits=10, decimal_places=2)
