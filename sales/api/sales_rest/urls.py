from django.urls import path
from .views import sales_list, sales_persons_history, api_customer, api_sales_person, api_show_autombile_vos

urlpatterns = [
    path("sales/", sales_list, name="sales_list"),
    path("sales-persons-history/<int:sales_person_id>/", sales_persons_history, name="sales_persons_history"),
    path("customers/", api_customer, name="create_customer"),
    path("salesperson/", api_sales_person, name="create_sales_person"),
    path("automobiles/", api_show_autombile_vos, name="api_show_autombile_vos"),
]
