from django.shortcuts import render
from django.http import JsonResponse
from common.json import ModelEncoder
from .models import AutoMobileVO, SalesPerson, PotentialCustomer, SaleRecord
from django.views.decorators.http import require_http_methods
import json

# Create your views here.


class AutoMobileVOEncoder(ModelEncoder):
    model = AutoMobileVO
    properties = [
        "id",
        "vin",
    ]


class SalesPersonEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["name", "employee_number"]


class PotentialCustomerEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = ["name"]


class SaleRecordEncoder(ModelEncoder):
    model = SaleRecord
    properties = [
        "sales_person",
        "customer",
        "automobile",
        "sale_price",
    ]
    encoders = {
        "automobile": AutoMobileVOEncoder(),
        "sales_person": SalesPersonEncoder(),
        "customer": PotentialCustomerEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_customer(request):
    if request.method == "GET":
        customer_VOs = PotentialCustomer.objects.all()
        customers = []
        for customer in customer_VOs:
            customers.append(
                {
                    "id": customer.id,
                    "name": customer.name,
                    "adress": customer.address,
                    "phone_number": customer.phone_number,
                }
            )
        return JsonResponse({"customers": customers})
    else:
        content = json.loads(request.body)
        customer = PotentialCustomer.objects.create(**content)
        return JsonResponse(customer, encoder=PotentialCustomerEncoder, safe=False)


@require_http_methods(["GET", "POST"])
def api_sales_person(request):
    if request.method == "GET":
        sales_person_VOs = SalesPerson.objects.all()
        sales_persons = []
        for sales_person in sales_person_VOs:
            sales_persons.append(
                {
                    "id": sales_person.id,
                    "name": sales_person.name,
                    "employee_number": sales_person.employee_number,
                }
            )
        return JsonResponse({"sales_persons": sales_persons})
    else:
        content = json.loads(request.body)
        sales_person = SalesPerson.objects.create(**content)
        return JsonResponse(sales_person, encoder=SalesPersonEncoder, safe=False)


@require_http_methods(["GET", "POST"])
def sales_list(request):
    # SalesPerson's name & employee_number, PotentialCustomer's name, vin, sale_price
    if request.method == "GET":
        sales_records = SaleRecord.objects.all()
        return JsonResponse({"sales": sales_records}, encoder=SaleRecordEncoder)
    else:  # POST
        content = json.loads(request.body)
        AutoMobileVO.objects.filter(id=content["automobile_id"]).update(sold=True)
        sales_record = SaleRecord.objects.create(**content)
        return JsonResponse(sales_record, encoder=SaleRecordEncoder, safe=False)


@require_http_methods(["GET"])
def sales_persons_history(request, sales_person_id):
    # show a list of sales for specified sales person's employee number(this will be down in the front end while backend will just send all employee numbers)
    # List shows SalesPerson's name, PotentialCustomer's name, AutoMobile's vin, sales_price
    sale_records = SaleRecord.objects.all()
    sale_persons_records = []
    for sale_persons_record in sale_records:
        if sale_persons_record.sales_person.id == sales_person_id:
            sale_persons_records.append(sale_persons_record)
    return JsonResponse(
        {"sale_persons_records": sale_persons_records}, encoder=SaleRecordEncoder
    )


@require_http_methods(["GET"])
def api_show_autombile_vos(request):
    automobile_VOs = AutoMobileVO.objects.all()
    automobiles = []
    for automobile in automobile_VOs:
        if automobile.sold == False:
            automobiles.append(
                {"id": automobile.id, "vin": automobile.vin}
            )
    return JsonResponse({"automobiles": automobiles}, encoder=AutoMobileVOEncoder)
