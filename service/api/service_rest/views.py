from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from common.json import ModelEncoder
import json
from .models import AutomobileVO, CustomerVO, TechnicianVO, Appointment

# Create your views here.


class TechnicianVOEncoder(ModelEncoder):
    model = TechnicianVO
    properties = ["name", "employee_number"]


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["vin"]


class CustomerVOEncoder(ModelEncoder):
    model = CustomerVO
    properties = ["name"]


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href", "vin"]


class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["date", "reason", "customer", "automobile", "technician"]
    encoders = {
        "customer": CustomerVOEncoder(),
        "automobile": AutomobileVOEncoder(),
        "technician": TechnicianVOEncoder(),
    }


class TechnicianVOListEncoder(ModelEncoder):
    model = TechnicianVO
    properties = ["name", "employee_number"]
    encoders = {
        "customer": CustomerVOEncoder(),
        "automobile": AutomobileVOEncoder(),
        "technician": TechnicianVOEncoder(),
    }


class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = ["customer", "date", "reason"]


class HistoryEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "vin",
        "customer_name",
        "date",
        "time",
        "technician",
        "reason",
    ]
    encoders = {"technician": TechnicianVOEncoder()}


class TechnicianDetailEncoder(ModelEncoder):
    model = TechnicianVO()
    properties = ["customer", "date", "reason"]


@require_http_methods(["GET"])
def api_show_service(request):
    appointment = Appointment.objects.all()
    return JsonResponse(
        {"appointment": appointment},
        encoder=AppointmentListEncoder,
        safe=False,
    )


@require_http_methods(["GET"])
def api_customer(request):
    customer = CustomerVO.objects.all()
    return JsonResponse(
        {"customer": customer},
        encoder=CustomerVOEncoder,
        safe=False,
    )


@require_http_methods(["GET"])
def api_automobile(request):
    automobile = Automobile.objects.all()
    return JsonResponse(
        {"automobile": automobile},
        encoder=AutomobileVOEncoder,
        safe=False,
    )


@require_http_methods(["GET", "POST"])
def api_technician(request):
    if request.method == "GET":
        technician = TechnicianVO.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianVOEncoder,
        )
    else:
        content = json.loads(request.body)
        technician = TechnicianVO.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianVOEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def show_technician(request):
    if request.method == "GET":
        technician = TechnicianVO.objects.all()
        return JsonResponse(
            {"technician": technician},
            encoder=TechnicianVOEncoder,
        )
    elif request.method == "DELETE":
        count, _ = TechnicianVO.objects.filter(id=pk).update(**content)
        return JsonResponse({"Deleted": count > 0})
        content = json.loads(request.body)
        technician = TechnicianVO.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianVOEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_appointment(request):
    if request.method == "GET":
        appointment = Appointment.objects.all()
        return JsonResponse(
            {"appointment": appointment},
            encoder=AppointmentListEncoder,
        )
    else:
        content = json.loads(request.body)
        # try:

        # if hasattr(content, "vip"):
        #     del content["vip"]
        # if hasattr(content, "is_finished"):
        #     del content["is_finished"]

        automobile: AutomobileVO.objects.get(id=content["automobile_id"])
        appointment = Appointment.objects.create(**content)
        return JsonResponse(
            appointment,
            encoder=AppointmentListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_show_appointment(request, pk):
    appointment = Appointment.objects.get(id=pk)
    return JsonResponse({"deleted": count > 0})


@require_http_methods(["GET"])
def api_show_service_history(request, pk):
    if request.method == "GET":
        history = Appointment.objects.filter(vin=pk)
        if history == []:
            return JsonResponse(
                {"message": "Invalid vin"},
                status=404,
                safe=False,
            )
        else:
            return JsonResponse(history, encoder=HistoryEncoder, safe=False)
