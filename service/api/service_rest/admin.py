from django.contrib import admin
from .models import CustomerVO, AutomobileVO, TechnicianVO, Appointment

# Register your models here.


admin.site.register(CustomerVO)
admin.site.register(AutomobileVO)
admin.site.register(TechnicianVO)
admin.site.register(Appointment)
