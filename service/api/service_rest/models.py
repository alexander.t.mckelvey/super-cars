from django.db import models
import datetime

# Create your models here.


class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return f"{self.vin}"


class CustomerVO(models.Model):
    name = models.CharField(max_length=200)
    address = models.CharField(max_length=200)


class TechnicianVO(models.Model):
    name = models.CharField(max_length=200)
    employee_number = models.PositiveIntegerField()


class Appointment(models.Model):
    date = models.DateTimeField(auto_now_add=True)
    reason = models.TextField(max_length=36, blank=True)

    customer = models.ForeignKey(
        CustomerVO,
        on_delete=models.CASCADE,
        related_name="appointment",
    )

    technician = models.ForeignKey(
        TechnicianVO,
        on_delete=models.CASCADE,
        related_name="appointment",
    )

    automobile = models.ForeignKey(
        AutomobileVO,
        on_delete=models.CASCADE,
        related_name="appointment",
    )

#   def set_vip_status(self, *args, **kwargs):
#     dealership_car = AutomobileVO.objects.filter(vin=self.vin)
#     self.vip = len(dealership_car) > 0
#     super().save(*args, **kwargs)


    # def get_api_url(self):
    #     return reverse("api_show_appointment", kwargs={"id": self.id})

    # def __str__(self):
    #     return self.name

    # class Meta:
    #     ordering = "date"  # Default ordering for Appointment
