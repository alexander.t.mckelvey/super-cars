from django.urls import path

from .views import (
    api_list_appointment,
    api_show_appointment,
    api_customer,
    api_technician,
    api_automobile,
    show_service_history,
)

urlpatterns = [
    path("appointments/", api_list_appointment, name="api_list_appointment"),
    path("technicians/", api_technician, name="api_technician"),
    path("technicians/", show_technician, name="show_technician"),
    path("customers/", api_customer, name="api_customer"),
    path("technicians/", api_technician, name="api_technician"),
    path("automobile/", api_automobile, name="api_automobile"),
    path("service-history/", api_show_appointment, name="api_show_service"),
    path("service-history/", show_service_history, name="show_service_history"),
]
