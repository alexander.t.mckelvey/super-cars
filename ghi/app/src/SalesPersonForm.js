import React, { useEffect, useState } from 'react';

const SalesPersonForm = () => {

    const [salesPersonName, setSalesPersonName] = useState('');
    const [salesPersonEmployeeNumber, setSalesPersonEmployeeNumber] = useState('');

    const handleSubmit = (event) => {
        event.preventDefault();
        const newSalesPerson = {
            "name": salesPersonName,
            "employee_number": salesPersonEmployeeNumber
        }

        const salesPersonURL = 'http://localhost:8090/api/salesperson/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newSalesPerson),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        fetch(salesPersonURL, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setSalesPersonName('');
                setSalesPersonEmployeeNumber('');
            })
            .catch(e => console.log('error: ', e));
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setSalesPersonName(value);
    }

    const handleEmployeeNumberChange = (event) => {
        const value = event.target.value;
        setSalesPersonEmployeeNumber(value)
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Sales Person</h1>
                    <form onSubmit={handleSubmit} id="create-sales-person-form">
                        <div className="form-floating mb-3">
                            <input value={salesPersonName} onChange={handleNameChange} required type="text" name="name" id="name" className="form-control" />
                            <label>Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={salesPersonEmployeeNumber} onChange={handleEmployeeNumberChange} required type="number" name="employee_number" id="employee_number" className="form-control" />
                            <label>Employee Number</label>
                        </div>
                        <button className='btn btn-primary'>Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SalesPersonForm
