import React, { useEffect, useState } from 'react';

const SalesRecordForm = () => {

    const [salesRecordAutomobile, setsalesRecordAutomobile] = useState('');
    const [salesRecordSalesPerson, setsalesRecordSalesPerson] = useState('');
    const [salesRecordCustomer, setsalesRecordCustomer] = useState('');
    const [salesRecordSalesPrice, setsalesRecordSalesPrice] = useState('');
    const [automobiles, setAutomobile] = useState([]);
    const [salesPersons, setSalesPerson] = useState([]);
    const [customers, setCustomer] = useState([]);

    useEffect(() => {
        const automobileURL = 'http://localhost:8090/api/automobiles/';
        fetch(automobileURL)
            .then(response => response.json())
            .then(data => setAutomobile(data.automobiles))
            .catch(e => console.error('error: ', e))

        const salesPersonURL = 'http://localhost:8090/api/salesperson/';
        fetch(salesPersonURL)
            .then(response => response.json())
            .then(data => setSalesPerson(data.sales_persons))
            .catch(e => console.error('error: ', e))

        const customerURL = 'http://localhost:8090/api/customers/';
        fetch(customerURL)
            .then(response => response.json())
            .then(data => setCustomer(data.customers))
            .catch(e => console.error('error: ', e))
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault();
        const newSalesRecord = {
            "automobile_id": salesRecordAutomobile,
            "sales_person_id": salesRecordSalesPerson,
            "customer_id": salesRecordCustomer,
            "sale_price": salesRecordSalesPrice
        }

        const salesRecordURL = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newSalesRecord),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        fetch(salesRecordURL, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setsalesRecordAutomobile('');
                setsalesRecordSalesPerson('');
                setsalesRecordCustomer('');
                setsalesRecordSalesPrice('');
            })
            .catch(e => console.log('error: ', e));
    }

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setsalesRecordAutomobile(value);
    }

    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setsalesRecordSalesPerson(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setsalesRecordCustomer(value);
    }

    const handleSalesPriceChange = (event) => {
        const value = event.target.value;
        setsalesRecordSalesPrice(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Sales Record</h1>
                    <form onSubmit={handleSubmit} id="create-sales-record-form">
                        <div className="form-floating mb-3">
                            <select onChange={handleAutomobileChange} value={salesRecordAutomobile} required id="automobile" name="automobile" className="form-select">
                                <option value="">Choose a Automobile</option>
                                {automobiles.map(automobile => {
                                    return (
                                        <option key={automobile.id} value={automobile.id}>{automobile.vin}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleSalesPersonChange} value={salesRecordSalesPerson} required id="sales_person" name="sales_person" className="form-select">
                                <option value="">Choose a Sales Person</option>
                                {salesPersons.map(sales_person => {
                                    return (
                                        <option key={sales_person.id} value={sales_person.id}>{sales_person.name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleCustomerChange} value={salesRecordCustomer} required id="customer" name="customer" className="form-select">
                                <option value="">Choose a Customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>{customer.name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={salesRecordSalesPrice} onChange={handleSalesPriceChange} required type="number" name="sales_price" id="sales_price" min="0" max="99999999" className="form-control" />
                            <label>Sales Price</label>
                        </div>
                        <button className='btn btn-primary'>Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default SalesRecordForm
