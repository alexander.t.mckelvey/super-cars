import { useEffect, useState } from "react";
import './index.css';

const ManufacturerList = () => {
    const [manufacturers, setManufacturers] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8100/api/manufacturers/')
            .then(response => response.json())
            .then(data => {
                setManufacturers(data.manufacturers);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    const onDeleteManufacturerClick = (manufacturer) => {
        const manufacturerHref = manufacturer.href;
        const hrefComponents = manufacturer.href.split('/');
        const pk = hrefComponents[hrefComponents.length - 2];
        const manufacturersUrl = `http://localhost:8100/api/manufacturers/${pk}/`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(manufacturersUrl, fetchConfig)
            .then(response => response.json())
            .then(data => {
                if (data.deleted) {
                    const currentManufacturers = [...manufacturers];
                    setManufacturers(currentManufacturers.filter(manufacturer => manufacturer.href !== manufacturerHref));
                }
            })
            .catch(e => console.log('error: ', e));
}

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers.map(manufacturer => {
                        return (
                            <tr key={manufacturer.href}>
                                {/* <td>
                                    <button onClick={() => onDeleteManufacturerClick(manufacturer)}>X</button>
                                    <span>{manufacturer.model}</span>
                                </td> */}
                                <td>{manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
};

export default ManufacturerList;