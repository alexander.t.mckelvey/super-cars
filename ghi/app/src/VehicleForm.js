import React, { useEffect, useState } from 'react';

const VehicleForm = () => {

    const [vehicleName, setVehicleName] = useState('');
    const [vehicleManufacturer, setVehicleManufacturer] = useState('');
    const [vehiclePicUrl, setVehiclePicUrl] = useState('');
    const [manufacturers, setManufacturers] = useState([]);

    useEffect(() => {
        const manufacturerVOsURL = 'http://localhost:8100/api/manufacturers/';
        fetch(manufacturerVOsURL)
            .then(response => response.json())
            .then(data => setManufacturers(data.manufacturers))
            .catch(e => console.error('error: ', e))
    }, [])

    const handleSubmit = (event) => {
        event.preventDefault();
        const newVehicle = {
            'name': vehicleName,
            'picture_url': vehiclePicUrl,
            'manufacturer_id': vehicleManufacturer
        }

        const vehiclesUrl = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newVehicle),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(vehiclesUrl, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setVehicleName('');
                setVehiclePicUrl('');
                setVehicleManufacturer('');
            })
            .catch(e => console.log('error: ', e));
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setVehicleName(value);
    }

    const handlePicUrlChange = (event) => {
        const value = event.target.value;
        setVehiclePicUrl(value);
    }

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setVehicleManufacturer(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a New Vehicle</h1>
                    <form onSubmit={handleSubmit} id="create-bin-form">
                        <div className="form-floating mb-3">
                            <input value={vehicleName} onChange={handleNameChange} required type="text" name="name" id="name" className="form-control" />
                            <label>Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={vehiclePicUrl} onChange={handlePicUrlChange} required type="text" name="picture_url" id="picture_url" className="form-control" />
                            <label>PicUrl</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleManufacturerChange} value={vehicleManufacturer} required id="manufacturer" name="manufacturer" className="form-select">
                                <option value="">Choose a Manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default VehicleForm;
