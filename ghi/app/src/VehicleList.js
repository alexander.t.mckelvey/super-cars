import { useEffect, useState } from "react";
import './index.css';

const VehicleList = () => {
    const [models, setModels] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8100/api/models/')
            .then(response => response.json())
            .then(data => {
                setModels(data.models);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    return (
        <>
            <h1>Vehicle Models</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Manufacturer</th>
                        <th>Picture</th>
                    </tr>
                </thead>
                <tbody>
                    {models.map(model => {
                        return (
                            <tr key={model.id}>
                                <td>{model.name}</td>
                                <td>{model.manufacturer.name}</td>
                                <td>{model.picture_url}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default VehicleList;
