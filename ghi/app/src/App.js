import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import AppointmentList from './AppointmentList';
import AppointmentForm from './AppointmentForm';
import TechnicianForm from './TechnicianForm';

import AutomobileForm from './AutomobileForm';
import Automobilelist from './Automobilelist';
import ManufacturerForm from './ManufacturerForm';
import ManufacturerList from './ManufacturerList';
import VehicleForm from './VehicleForm';
import VehicleList from './VehicleList';

import PotentialCustomerForm from './PotentialCustomerForm';
import SalesPersonForm from './SalesPersonForm';
import SalesList from './SalesList';
import SalesRecordForm from './SaleRecordForm';
import SalesPersonHistory from './SalesPersonHistory';
import ServiceHistory from './ServiceHistory';


function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>

          <Route exact path="/" element={<MainPage />} />

          <Route path="appointments">
            <Route index element={<AppointmentList />} />
            <Route path="new" element={<AppointmentForm />} />
          </Route>

          <Route path="technicians">
            <Route index element={<TechnicianForm />} />
          </Route>

          <Route path="manufacturers">
            <Route index element={<ManufacturerList />} />
            <Route path="new" element={<ManufacturerForm />} />
          </Route>

          <Route path="models">
            <Route index element={<VehicleList />} />
            <Route path="new" element={<VehicleForm />} />
          </Route>

          <Route path="automobiles">
            <Route index element={<Automobilelist />} />
            <Route path='new' element={<AutomobileForm />} />
          </Route>

          <Route path="customers">
            <Route index element={<PotentialCustomerForm />} />
          </Route>

          <Route path="sales-person">
            <Route index element={<SalesPersonForm />} />
          </Route>

          <Route path="sales">
            <Route index element={<SalesList />} />
            <Route path='new' element={<SalesRecordForm />} />
          </Route>

          <Route path="sales-person-history">
            <Route index element={<SalesPersonHistory />} />
          </Route>

          <Route path="service-history">
            <Route index element={<ServiceHistory />} />
          </Route>

        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
