import React, { useEffect, useState } from 'react';

const TechnicianForm = () => {

    const [technicianName, setTechnicianName] = useState('');
    const [technicianEmployeeeNumber, setTechnicianEmployeeeNumber] = useState('');


    const handleSubmit = (event) => {
        event.preventDefault();
        const newTechnician = {
            'name': technicianName,
            'employee_number': technicianEmployeeeNumber,
        }

        const techniciansUrl = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newTechnician),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(techniciansUrl, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setTechnicianName('');
                setTechnicianEmployeeeNumber('');
            })
            .catch(e => console.log('error: ', e));
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setTechnicianName(value);
    }

    const handleEmployeeeNumberChange = (event) => {
        const value = event.target.value;
        setTechnicianEmployeeeNumber(value);
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input value={technicianName} onChange={handleNameChange} required type="text" name="name" id="name" className="form-control" />
                            <label>Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={technicianEmployeeeNumber} onChange={handleEmployeeeNumberChange} required type="text" name="employee_number" id="employee_number" className="form-control" />
                            <label>Employee Number</label>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default TechnicianForm;
