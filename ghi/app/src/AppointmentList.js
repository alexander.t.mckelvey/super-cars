import { useEffect, useState } from "react";
import './index.css';

const AppointmentList = () => {
    const [appointments, setAppointments] = useState([])

    useEffect(() => {
        fetch('http://localhost:8080/api/appointments/')
            .then(response => response.json())
            .then(data => {
                setAppointments(data.appointments);
            })
            .catch(e => console.log('error: ', e))
    }, [])

    const onDeleteAppointment = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/`;
        const fetchConfig = {
            method: "delete",
            headers: {
                'Content-Type': 'application/json',
            }
        }


        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            window.location.reload()
        }
    }

    const appointmentFinished = async (id) => {
        const url = `http://localhost:8080/api/appointments/${id}/`
        const fetchConfig = {
            method: "put",
            body: JSON.stringify({ finished: true }),
            headers: {
                'Content-Type': 'application/json',
            }
        }

        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            window.location.reload()
        }
    }

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                        <th>VIP</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments?.map((appointment) => {
                        if (!appointment.finished) {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.technician}</td>
                                    <td>{appointment.reason}</td>
                                    <td>{appointment.vip}</td>
                                    <td>
                                        <form>
                                            <button onClick={() => appointmentFinished(appointment.id)}>Finished</button>
                                            <span>{appointment.model}</span>
                                        </form>
                                    </td>
                                    <td>
                                        <form>
                                            <button onClick={() => onDeleteAppointment(appointment.id)}>Cancel</button>
                                            <span>{appointment.model}</span>
                                        </form>
                                    </td>
                                </tr>
                            )
                        }
                    })}
                </tbody>
            </table>
        </>
    );
};
// };

export default AppointmentList;