import React, { useEffect, useState } from 'react';

const PotentialCustomerForm = () => {

    const [customerName, setCustomerName] = useState('');
    const [customerAdress, setCustomerAdress] = useState('');
    const [customerPhoneNumber, setCustomerPhoneNumber] = useState('');


    const handleSubmit = (event) => {
        event.preventDefault();
        const newCustomer = {
            "name": customerName,
            "address": customerAdress,
            "phone_number": customerPhoneNumber
        }

        const customersURL = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newCustomer),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        fetch(customersURL, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setCustomerName('');
                setCustomerAdress('');
                setCustomerPhoneNumber('');
            })
            .catch(e => console.log('error: ', e));
    }

    const handleNameChange = (event) => {
        const value = event.target.value;
        setCustomerName(value);
    }

    const handleAdressChange = (event) => {
        const value = event.target.value;
        setCustomerAdress(value);
    }

    const handlePhoneNumberChange = (event) => {
        const value = event.target.value;
        setCustomerPhoneNumber(value);
    }

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a Potential Customer</h1>
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="form-floating mb-3">
                            <input value={customerName} onChange={handleNameChange} required type="text" name="name" id="name" className="form-control" />
                            <label>Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={customerAdress} onChange={handleAdressChange} required type="text" name="adress" id="adress" className="form-control" />
                            <label>Adress</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={customerPhoneNumber} onChange={handlePhoneNumberChange} required type="text" name="phone_number" id="phone_number" className="form-control" />
                            <label>Phone Number</label>
                        </div>
                        <button className='btn btn-primary'>Create</button>
                    </form>
                </div>
            </div>
        </div>
    )
}

export default PotentialCustomerForm
