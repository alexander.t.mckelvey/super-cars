import React, { useEffect, useState } from 'react';

const AutomobileForm = () => {

    const [automobileColor, setAutomobileColor] = useState('');
    const [automobileYear, setAutomobileYear] = useState('');
    const [automobileVin, setAutomobileVin] = useState('');
    const [automobileModel, setAutomobileModel] = useState('');
    const [models, setModels] = useState([]);

    useEffect(() => {
        const automobileVOsURL = 'http://localhost:8100/api/models/';
        fetch(automobileVOsURL)
            .then(response => response.json())
            .then(data => setModels(data.models))
            .catch(e => console.error('error: ', e))
    }, [])


    const handleSubmit = (event) => {
        event.preventDefault();
        const newAutomobile = {
            'color': automobileColor,
            'year': automobileYear,
            'vin': automobileVin,
            'model_id': automobileModel,
        }

        const automobilesUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newAutomobile),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(automobilesUrl, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setAutomobileColor('');
                setAutomobileYear('');
                setAutomobileVin('');
                setAutomobileModel('');
            })
            .catch(e => console.log('error: ', e));
    }

    const handleColorChange = (event) => {
        const value = event.target.value;
        setAutomobileColor(value);
    }

    const handleYearChange = (event) => {
        const value = event.target.value;
        setAutomobileYear(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setAutomobileVin(value);
    }

    const handleModelChange = (event) => {
        const value = event.target.value;
        setAutomobileModel(value);
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add an automobile to inventory</h1>
                    <form onSubmit={handleSubmit} id="create-automobile-form">
                        <div className="form-floating mb-3">
                            <input value={automobileColor} onChange={handleColorChange} required type="text" name="color" id="color" className="form-control" />
                            <label>Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={automobileYear} onChange={handleYearChange} required type="text" name="year" id="year" className="form-control" />
                            <label>Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input value={automobileVin} onChange={handleVinChange} required type="text" name="vin" id="vin" className="form-control" />
                            <label>VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleModelChange} value={automobileModel} required id="model" name="model" className="form-select">
                                <option value="">Choose a model</option>
                                {models.map(model => {
                                    return (
                                        <option key={model.id} value={model.id}>{model.name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default AutomobileForm;