import { useEffect, useState } from "react";
import './index.css';

const AutomobileList = () => {
    const [automobiles, setAutombiles] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8100/api/automobiles/')
            .then(response => response.json())
            .then(data => {
                setAutombiles(data.autos);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    return (
        <>
            <h1>Automobiles</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Color</th>
                        <th>Year</th>
                        <th>Model</th>
                        <th>Manufacturer</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles.map(automobile => {
                        return (
                            <tr key={automobile.id}>
                                <td>{automobile.vin}</td>
                                <td>{automobile.color}</td>
                                <td>{automobile.year}</td>
                                <td>{automobile.model.name}</td>
                                <td>{automobile.model.manufacturer.name}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default AutomobileList;
