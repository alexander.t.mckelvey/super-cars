import { useEffect, useState } from "react";
import './index.css';

const SalesPersonHistory = () => {
    const [id, setId] = useState('');
    const [sales, setSales] = useState([]);
    const [salesPerson, setSalesPerson] = useState([]);

    useEffect(() => {
        const salesPersonURL = 'http://localhost:8090/api/salesperson/';
        fetch(salesPersonURL)
            .then(response => response.json())
            .then(data => setSalesPerson(data.sales_persons))
            .catch(e => console.error('error: ', e))

        const salesPersonHistoryURL = `http://localhost:8090/api/sales-persons-history/${id}/`
        fetch(salesPersonHistoryURL)
            .then(response => response.json())
            .then(data => {
                setSales(data.sale_persons_records);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    const handleSalesPersonChange = (event) => {
        const value = event.target.value;
        setId(value);
    }

    return (
        <>
            <h1>Sales person history</h1>
            <select onChange={handleSalesPersonChange} value={id} required id="sales_person" name="sales_person" className="form-select">
                <option value="">Choose a Sales Person</option>
                {salesPerson.map(sales_person => {
                    return (
                        <option key={sales_person.id} value={sales_person.id}>{sales_person.name}</option>
                    );
                })}
            </select>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Customer</th>
                        <th>Automobile VIN</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.sales_person.name}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.sale_price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default SalesPersonHistory;
