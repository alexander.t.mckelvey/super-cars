import React, { useEffect, useState } from 'react';

const AppointmentForm = () => {
    const [appointmentDate, setAppointmentDate] = useState('');
    // const [appointmentTime, setAppointmentTime] = useState('');
    const [appointmentReason, setAppointmentReason] = useState('');
    const [appointmentCustomer, setAppointmentCustomer] = useState('');
    const [appointmentAutomobile, setAppointmentAutomobile] = useState('');
    const [appointmentTechnician, setAppointmentTechnician] = useState('');
    const [technicians, setTechnicians] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);

    useEffect(() => {

        const automobilesURL = 'http://localhost:8100/api/automobiles/';
        fetch(automobilesURL)
            .then(response => response.json())
            .then(data => setAutomobiles(data.automobile))
            .catch(e => console.error('error: ', e))

        const techniciansURL = 'http://localhost:8080/api/technicians/';
        fetch(techniciansURL)
            .then(response => response.json())
            .then(data => setTechnicians(data.technician))
            .catch(e => console.error('error: ', e))

        const customersURL = 'http://localhost:8080/api/customers/';
        fetch(customersURL)
            .then(response => response.json())
            .then(data => setCustomers(data.customer))
            .catch(e => console.error('error: ', e))
    }, [])


    const handleSubmit = (event) => {
        event.preventDefault();
        const newAppointment = {
            'date': appointmentDate,
            // 'time': appointmentTime,
            'reason': appointmentReason,
            'customer_id': appointmentCustomer,
            'automobile_id': appointmentAutomobile,
            'technician_id': appointmentTechnician
        }

        const appointmentsUrl = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(newAppointment),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        fetch(appointmentsUrl, fetchConfig)
            .then(response => response.json())
            .then(() => {
                setAppointmentDate('');
                // setAppointmentTime('');
                setAppointmentReason('');
                setAppointmentCustomer('');
                setAppointmentAutomobile('');
                setAppointmentTechnician('');
            })
            .catch(e => console.log('error: ', e));
    }

    const handleDateChange = (event) => {
        const value = event.target.value;
        setAppointmentDate(value);
    }

    // const handleTimeChange = (event) => {
    //     const value = event.target.value;
    //     setAppointmentTime(value);
    // }

    const handleReasonChange = (event) => {
        const value = event.target.value;
        setAppointmentReason(value);
    }

    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setAppointmentCustomer(value);
    }

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAppointmentAutomobile(value);
    }

    const handleTechnicianChange = (event) => {
        const value = event.target.value;
        setAppointmentTechnician(value);
    }
    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new appointment</h1>
                    <form onSubmit={handleSubmit} id="create-appointment-form">
                        <div className="form-floating mb-3">
                            <input value={appointmentDate} onChange={handleDateChange} required type="date" name="date" id="date" className="form-control" />
                            <label>Date</label>
                        </div>
                        {/* <div className="form-floating mb-3">
                            <input value={appointmentTime} onChange={handleTimeChange} required type="time" name="time" id="time" className="form-control" />
                            <label>Time</label>
                        </div> */}
                        <div className="form-floating mb-3">
                            <input value={appointmentReason} onChange={handleReasonChange} required type="text" name="reason" id="reason" className="form-control" />
                            <label>Reason</label>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleCustomerChange} value={appointmentCustomer} required id="customer" name="customer" className="form-select">
                                <option value="">Choose a customer</option>
                                {customers.map(customer => {
                                    return (
                                        <option key={customer.id} value={customer.id}>{customer.name}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            {/* comment out line 126 - 145 to see form (without commented out content obviously) */}
                            <select onChange={handleAutomobileChange} value={appointmentAutomobile} required id="automobile" name="automobile" className="form-select">
                                <option value="">Choose an automobile</option>
                                {automobiles.map(automobile => {
                                    return (
                                        <option key={automobile.id} value={automobile.id}>{automobile.vin}</option>
                                    );
                                })}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <select onChange={handleTechnicianChange} value={appointmentTechnician} required id="technician" name="technician" className="form-select">
                                <option value="">Choose a technician</option>
                                {technicians.map(technician => {
                                    return (
                                        <option key={technician.id} value={technician.id}>{technician.name}</option>
                                    );
                                })}
                            </select>

                        </div>

                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div >
        </div >
    );
}

export default AppointmentForm;
