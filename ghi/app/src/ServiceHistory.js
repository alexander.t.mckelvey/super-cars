import { useEffect, useState } from "react";
import './index.css';

const ServiceHistory = () => {
    const [appointments, setAppointments] = useState([])

    useEffect(() => {
        const serviceHistoryURL = 'http://localhost:8080/api/service-history/';
        fetch(serviceHistoryURL)
            .then(response => response.json())
            .then(data => {
                setAppointments(data.service_history);
            })
            .catch(e => console.log('error: ', e))
    }, [])

    return (
        <>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>VIN</th>
                        <th>Customer</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Technician</th>
                        <th>Reason</th>
                    </tr>
                </thead>
                <tbody>
                    {appointments?.map((appointment) => {
                        if (!appointment.finished) {
                            return (
                                <tr key={appointment.id}>
                                    <td>{appointment.vin}</td>
                                    <td>{appointment.customer}</td>
                                    <td>{appointment.date}</td>
                                    <td>{appointment.technician}</td>
                                    <td>{appointment.reason}</td>
                                    <td>
                                    </td>
                                </tr>
                            )
                        }
                    })}
                </tbody>
            </table>
        </>
    );
};
// };

export default ServiceHistory;