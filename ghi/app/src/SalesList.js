import { useEffect, useState } from "react";
import './index.css';

const SalesList = () => {
    const [sales, setSales] = useState([]);

    useEffect(() => {
        fetch('http://localhost:8090/api/sales/')
            .then(response => response.json())
            .then(data => {
                setSales(data.sales);
            })
            .catch(e => console.log('error: ', e));
    }, [])

    return (
        <>
            <h1>Sales List</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Sales Person</th>
                        <th>Employee Number</th>
                        <th>Customer</th>
                        <th>Automobile VIN</th>
                        <th>Sale Price</th>
                    </tr>
                </thead>
                <tbody>
                    {sales.map(sale => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.sales_person.name}</td>
                                <td>{sale.sales_person.employee_number}</td>
                                <td>{sale.customer.name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.sale_price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </>
    );
}

export default SalesList;
