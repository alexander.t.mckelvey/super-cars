# CarCar

Team:

* Lauren Alpuerto - Services
* Alexander McKelvey - Sales

## Service microservice

Explain your models and integration with the inventory
microservice, here.

**How to Run this Application**
1. use commands to set up docker:
    *docker volume create beta-data
    *docker-compose build
    *docker-compose up

2. In a browser tab open up "http://localhost:3000/"

**API Documentation**

3. Create technician - http://localhost:8080/api/technicians/
    - POST
    - JSON
    {
      "name": "lauren",
      "employee_number": 2
    }
  
  Enter a service appointment - http://localhost:8080/api/appointments/
    - POST
    - JSON
    {
    "date": "2022-12-05T01:26:30+00:00",
    "reason": "hiuhlkjhgk",
    "customer_id": 1,
    "technician_id": 1,
    "automobile_id": 1
  }
  List appointments - http://localhost:8080/api/appointments/
    - GET
    {
	"appointment": []
    } 
  Service history - http://localhost:8080/api/service-history/<int:id>/
    - GET
    {
    "vin": "2022-12-05T01:26:30+00:00",
    "customer_id": "hiuhlkjhgk",
    "date": 1,
    "time": 1,
    "technician": 1
    "reason": 1
    }

4.  Value Objects: AutomobileVO, CustomerVO, TechnicianVO

**Application Diagram**
https://excalidraw.com/#json=LHRUBLuwRNMza1hObgwPN,JjwzPCr7OOqbiiz32CqfxQ


## Sales microservice

**How to Run this Application**
1. use commands to set up docker:
    *docker volume create beta-data
    *docker-compose build
    *docker-compose up

2. In a browser tab open up "http://localhost:3000/"

3. In the nav bar at "http://localhost:3000/", there will be "Create Customer", "Create Sales Person", "Sale Records", "Create Sale Record", and "Sales Person's History"
    *You can create a customer and salesperson in those nav link, but to create a sale record you will have to create a automobile, which requires you to create a vehicle model, and the vehicle model will require you to create a manufacturer. All of these are in the nav bar as well.
    *You can use Sale Records nav link to see a list of all the Sale Records you created
    *You can use Sales Person's History nav link to see a list of sale records that were made with a specified Sales Person that you select

**Application Diagram**
https://excalidraw.com/#json=LHRUBLuwRNMza1hObgwPN,JjwzPCr7OOqbiiz32CqfxQ

**API Documentation**
SALES APIS
1. List Sales(GET, POST)
http://localhost:8090/api/sales/
{
	"automobile_id": 1 ,
	"sales_person_id": 1,
	"customer_id": 1,
	"sale_price": "5000.00"
}

2. List Customers(GET, POST)
http://localhost:8090/api/customers/
{
    "name": "jack",
    "address": "5643 W Charleston Blvd",
    "phone_number": "123-123-1234"
}

3. List Sales Person(GET, POST)
http://localhost:8090/api/salesperson/
{
    "name": "bob",
    "employee_number": 9
}

4. List Sales Persons History(GET)
http://localhost:8090/api/sales-persons-history/<int:id>/

**Value Objects**

automobile
customer
salesperson
saleprice




## Inventory APIs
1. Manufacturers List and Create(GET, POST)
http://localhost:8100/api/manufacturers/
{
  "name": "Chrysler"
}

2. Manufacturers Detail, Update, and Delete(GET, PUT, DELETE)
http://localhost:8100/api/manufacturers/<int:id>/

3. Vehicle Models List and Create(GET, POST)
http://localhost:8100/api/models/
{
  "name": "Sebring",
  "picture_url": "https://upload.wikimedia.org/wikipedia/commons/thumb/7/71/Chrysler_Sebring_front_20090302.jpg/320px-Chrysler_Sebring_front_20090302.jpg",
  "manufacturer_id": 1
}

4. Vehicle Models Detail, Update, and Delete(GET, PUT, DELETE)
http://localhost:8100/api/models/<int:id>/

5. Automobiles List and Create(GET, POST)
http://localhost:8100/api/automobiles/
{
  "color": "red",
  "year": 2012,
  "vin": "1C3CC5FB2AN120174",
  "model_id": 1
}

6. Automobiles Detail, Update, and Delete(GET, PUT, DELETE)
http://localhost:8100/api/automobiles/<int:vin>/
